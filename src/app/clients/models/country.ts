export class Country {
  constructor(
    public  id: number,
    public name?: string,
    public code?: string,
    public flag?: File,
    public display?: boolean
  ) {}
}
