import {Currency} from './currency';
import {Branch} from '../../branches/models/branch';
import {Country} from './country';

export class Client {
  constructor(
    public id: number,
    public currencies?: Array<Currency>,
    public branch?: Branch,
    public country?: Country,
    public cName?: string,
    public code?: string,
    public stree1?: string,
    public street2?: string,
    public city?: string,
    public zipCode?: string,
    public state?: string,
    public website?: string,
    public vatNumber?: string,
    public marketingLink?: string,
    public minimumFree?: number,
    public email1?: string,
    public email2?: string,
    public phone1?: string,
    public phone2?: string,
    public phone3?: string,
    public phone4?: string,
    public fax?: string,
    public clientCats?: string,
  ) {}
}
