import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import { BranchesComponent } from './branches/branches.component';
import { ClientsComponent } from './clients/clients.component';
import { DaemonComponent } from './daemon/daemon.component';
import { FreelancesComponent } from './freelances/freelances.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { OldqmComponent } from './oldqm/oldqm.component';
import { PlanningComponent } from './planning/planning.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { ProjectsComponent } from './projects/projects.component';
import { QmComponent } from './qm/qm.component';
import { SecruitmentComponent } from './secruitment/secruitment.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {PlanningDialogComponent} from './employees/profile/dialogs/planning-dialog/planning-dialog.component';
import { CreateEntretienDialogComponent } from './employees/profile/dialogs/create-entretien-dialog/create-entretien-dialog.component';
import { ListEntretienDialogComponent } from './employees/profile/dialogs/list-entretien-dialog/list-entretien-dialog.component';
import { ListEvaluationDialogComponent } from './employees/profile/dialogs/list-evaluation-dialog/list-evaluation-dialog.component';
import { ListProjectDialogComponent } from './employees/profile/dialogs/list-project-dialog/list-project-dialog.component';
import { ListRemarqueDialogComponent } from './employees/profile/dialogs/list-remarque-dialog/list-remarque-dialog.component';
import {ConfirmDeleteProjectDialogComponent } from './employees/profile/dialogs/list-remarque-dialog/confirm-delete-remarque-dialog/confirm-delete-project-dialog.component';
import { AddRemarqueDialogComponent } from './employees/profile/dialogs/list-remarque-dialog/add-remarque-dialog/add-remarque-dialog.component';
import {EmployeesModule} from './employees/employees.module';
import {ClientsModule} from './clients/clients.module';
import { PoFactureComponent } from './PO_FACTURES/po-facture/po-facture.component';
import {PoFacturesModule} from './PO_FACTURES/po-factures.module';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    BranchesComponent,
    ClientsComponent,
    DaemonComponent,
    FreelancesComponent,
    InvoiceComponent,
    OldqmComponent,
    PlanningComponent,
    ProfilesComponent,
    ProjectsComponent,
    QmComponent,
    SecruitmentComponent,
    MainNavComponent,
    PlanningDialogComponent,
    CreateEntretienDialogComponent,
    ListEntretienDialogComponent,
    ListEvaluationDialogComponent,
    ListProjectDialogComponent,
    ListRemarqueDialogComponent,
    ConfirmDeleteProjectDialogComponent,
    AddRemarqueDialogComponent,
    PoFactureComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    EmployeesModule,
    ClientsModule,
    PoFacturesModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [],
  entryComponents: [
    PlanningDialogComponent,
    CreateEntretienDialogComponent,
    ListEntretienDialogComponent,
    ListEvaluationDialogComponent,
    ListProjectDialogComponent,
    ListRemarqueDialogComponent,
    ConfirmDeleteProjectDialogComponent,
    AddRemarqueDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
