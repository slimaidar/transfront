import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OldqmComponent } from './oldqm.component';

describe('OldqmComponent', () => {
  let component: OldqmComponent;
  let fixture: ComponentFixture<OldqmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OldqmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OldqmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
