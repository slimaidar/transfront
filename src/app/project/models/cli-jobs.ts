export class CliJobs {
  constructor(
    public id: number,
    public code?: string,
    public intitule?: string,
    public tarif?: number,
    public volume?: number,
    public dateCreation?: Date
  ) {}
}
