import {ClientActions, ClientActionTypes} from './client.actions';
import {Client} from "../../clients/models/client";


export interface ClientState {
  clientId: number;
  navType: string;
  selected: boolean;
}

export const initialState: ClientState = {
  clientId: null,
  navType: 'cli',
  selected: false,
};

export function clientReducer(state = initialState, action: ClientActions): ClientState {
  switch (action.type) {
    case ClientActionTypes.SaveClientId:
      localStorage.setItem('clientId', action.clientId.toString())
      return { clientId: action.clientId, navType: 'cli', selected: true};
    case ClientActionTypes.RemoveClientId:
      return { clientId: null , navType: 'cli', selected: false};
    case ClientActionTypes.RouteType:
      return { clientId: null , navType: action['navType'], selected: false};
    default:
      return state;
  }
}
