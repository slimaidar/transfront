import { ClientTabActions, ClientTabActionTypes } from './client-tab.actions';
import { Client } from './../../../clients/models/client';


export interface ClientTabState {
  clients: Client[];
  loaded: boolean;
}

export const initialState: ClientTabState = {
  clients: [],
  loaded: false
};

export function clientTabReducer(state = initialState, action: ClientTabActions): ClientTabState {
  switch (action.type) {
    case ClientTabActionTypes.LoadClientTab:
      return { clients: action.clients, loaded: action.loaded }
    default:
      return state;
  }
}
