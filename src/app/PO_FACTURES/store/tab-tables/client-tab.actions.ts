import { Client } from './../../../clients/models/client';
import { Action } from '@ngrx/store';

export enum ClientTabActionTypes {
  LoadClientTab = '[ClientTab] Load ClientTab'
}

export class LoadClientTabs implements Action {
  readonly type = ClientTabActionTypes.LoadClientTab;

  constructor(public clients: Client[], public loaded: boolean) {}
}

export type ClientTabActions = LoadClientTabs;
