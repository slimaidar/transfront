import { tap } from 'rxjs/internal/operators';
import { LoadClientTabs, ClientTabActionTypes } from './client-tab.actions';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { INIT } from '@ngrx/store';
import { defer } from 'rxjs';


@Injectable()
export class ClientTabEffects {

  constructor(private actions$: Actions) {}
}
