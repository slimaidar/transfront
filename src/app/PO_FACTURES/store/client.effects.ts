import { Injectable } from '@angular/core';
import {Actions} from '@ngrx/effects';
import { Store} from '@ngrx/store';
import {State} from '../../reducers/index';


@Injectable()
export class ClientEffects {

  constructor(private actions$: Actions, private store: Store<State>) {}

}
