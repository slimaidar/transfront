import { Action } from '@ngrx/store';
import 'rxjs';

export enum ClientActionTypes {
  SaveClientId = '[Client] Save Client ID',
  RemoveClientId = '[Client] Remove Client ID',
  RouteType = '[Client] Route Type',
  ClientSelected = '[Client] Client Selected'
}

export class SaveClientId implements Action {
  readonly type = ClientActionTypes.SaveClientId;

  constructor(public clientId: number) {}
}

export class RemoveClientId implements Action {
  readonly type = ClientActionTypes.RemoveClientId;

  constructor() {}
}

export class RouteType implements Action {
  readonly type = ClientActionTypes.RouteType;

  constructor(public navType: string) {}
}

export class ClientSelected implements Action {
  readonly type = ClientActionTypes.ClientSelected;

  constructor(public selected: boolean) {}
}



export type ClientActions = SaveClientId | RemoveClientId | RouteType | ClientSelected;
