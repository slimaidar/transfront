import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {select, Store} from '@ngrx/store';
import {State} from '../../reducers/index';

@Component({
  selector: 'app-po-facture',
  templateUrl: './po-facture.component.html',
  styleUrls: ['./po-facture.component.scss']
})
export class PoFactureComponent implements OnInit {

  constructor(
    private store: Store<State>,
    private router: Router) {

  }

  disabled: boolean = false;
  type: string;

  ngOnInit() {
    this.store.pipe(
      select(state => state.clients)
    ).subscribe(
      res => {
        if (!res.selected) {
          // this.router.navigateByUrl('/po&facture/client/clients')
         setTimeout( () => {
          this.type = res.navType;
          this.disabled = false
         }, 0)
        } else {
          setTimeout( () => {
            this.type = res.navType;
            this.disabled = true
          }, 0)
        }
      }
    )
  }

}
