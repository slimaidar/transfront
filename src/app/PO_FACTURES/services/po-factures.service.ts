import { UiService } from './ui.service';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Client} from "../../clients/models/client";
import {Observable} from "rxjs/index";


@Injectable({
  providedIn: 'root'
})
export class PoFacturesService {

  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private uiService: UiService) { }

  // client
  public getClients() {
    this.uiService.loadingStateChanged.next(true);
    return this.http.get(this.apiUrl + '/clients/');
  }

  public updateClient(client: Client) {

  }

  public getClient(id: number) {
    return this.http.get(this.apiUrl + `/clients/${id}`);
  }

  // Contact
  public getContacts(idClient: number) {
    return this.http.get(this.apiUrl + `/clients/${idClient}/contacts`);
  }

  // contacts
  public getContact(idClient: number) {
    return this.http.get(this.apiUrl + `/clients/${idClient}/contacts`);
  }

  // clijobs
  public getCliJobs(idClient: number) {
    return this.http.get(this.apiUrl + `/clients/${idClient}/clijobs`);
  }

  public getFi() {
    return this.http.get(this.apiUrl + '/fi/');
  }

  public getFm() {
    return this.http.get(this.apiUrl + '/fm/');
  }


  //  Paiements
  public getPaiements() {
    return this.http.get(`${this.apiUrl}/paiements/`);
  }

  //branch
  public getBranches() {
    return this.http.get(`${this.apiUrl}/branches`)
  }

  public getBranchesByClient(clientId: number) {
    return this.http.get(`${this.apiUrl}/clients/${clientId}/branches`)
  }


  //currencies
  getCurrencies() {
    return this.http.get(`${this.apiUrl}/currencies`)
  }

  // countries
  getCountries() {
    return this.http.get(`${this.apiUrl}/countries`)
  }
}
