import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FreelancerService {

  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  public getFreelancers() {
    return this.http.get(this.apiUrl + '/freelancers');
  }

  public getFreeJobs() {
    return this.http.get(this.apiUrl + '/freejobs');
  }

  public getFactures() {
    return this.http.get(this.apiUrl + '/factures');
  }

}
