import { ClientTabEffects } from './store/tab-tables/client-tab.effects';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PoFacturesRoutingModule } from './po-factures-routing.module';
import {ClientsTableComponent} from './tab-tables/clients-table/clients-table.component';
import {FiTableComponent} from './tab-tables/fi-table/fi-table.component';
import {FmTableComponent} from './tab-tables/fm-table/fm-table.component';
import {PaiementsTableComponent} from './tab-tables/paiements-table/paiements-table.component';
import {
  MatButtonModule, MatDatepickerModule,
  MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatSelectModule,
} from '@angular/material';
import {ClientConsulterDialogComponent} from './tab-tables/clients-table/client-consulter-dialog/client-consulter-dialog.component';
import {ClientModifierDialogComponent} from './tab-tables/clients-table/client-modifier-dialog/client-modifier-dialog.component';
import {FilterFormComponent} from './tab-tables/filter-form/filter-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../material/material.module';
import {CliJobsComponent} from './tab-tables/cli-jobs/cli-jobs.component';
import {ClijobConsulterDialogComponent} from './tab-tables/cli-jobs/clijob-consulter-dialog/clijob-consulter-dialog.component';
import {ClijobFacturerDialogComponent} from './tab-tables/cli-jobs/clijob-facturer-dialog/clijob-facturer-dialog.component';
import {ClijobModifierDialogComponent} from './tab-tables/cli-jobs/clijob-modifier-dialog/clijob-modifier-dialog.component';
import {FiVisualiserDialogComponent} from './tab-tables/fi-table/fi-visualiser-dialog/fi-visualiser-dialog.component';
import {FiModifierDialogComponent} from './tab-tables/fi-table/fi-modifier-dialog/fi-modifier-dialog.component';
import {FmVisualiserDialogComponent} from './tab-tables/fm-table/fm-visualiser-dialog/fm-visualiser-dialog.component';
import {FmModifierDialogComponent} from './tab-tables/fm-table/fm-modifier-dialog/fm-modifier-dialog.component';
import {FmAddDialogComponent} from './tab-tables/fm-table/fm-add-dialog/fm-add-dialog.component';
import {PaiementAddDialogComponent} from './tab-tables/paiements-table/paiement-add-dialog/paiement-add-dialog.component';
import {PaiementModifierDialogComponent} from './tab-tables/paiements-table/paiement-modifier-dialog/paiement-modifier-dialog.component';
import {PaiementClotureDialogComponent} from './tab-tables/paiements-table/paiement-cloture-dialog/paiement-cloture-dialog.component';
import {ClientProfileComponent} from './tab-tables/clients-table/client-profile/client-profile.component';
import {ContactsComponent} from './tab-tables/contacts/contacts.component';
import {FilterFormShortComponent} from './tab-tables/filter-form-short/filter-form-short.component';
import {ContactProfileComponent} from './tab-tables/contacts/contact-profile/contact-profile.component';
import {QuotesComponent} from './tab-tables/quotes/quotes.component';
import {StatisticsComponent} from './tab-tables/statistics/statistics.component';
import {InvoicingMpComponent} from './tab-tables/invoicing-mp/invoicing-mp.component';
import {EffectsModule} from '@ngrx/effects';
import { FreelancersTabComponent } from './tab-tables-freelance/freelancers-tab/freelancers-tab.component';
import { FreejobsTabComponent } from './tab-tables-freelance/freejobs-tab/freejobs-tab.component';
import { FacturesTabComponent } from './tab-tables-freelance/factures-tab/factures-tab.component';
import { PaymentsTabComponent } from './tab-tables-freelance/payments-tab/payments-tab.component';
import { FilterFormByFreelanceComponent } from './tab-tables-freelance/filter-form-by-freelance/filter-form-by-freelance.component';
import {FreelanceConsulterDialogComponent} from "./tab-tables-freelance/freelancers-tab/freelance-consulter-dialog/freelance-consulter-dialog.component";
import { FreelanceFilterFormComponent } from './tab-tables-freelance/freelance-filter-form/freelance-filter-form.component';
import { PayeeDialogComponent } from './tab-tables-freelance/factures-tab/payee-dialog/payee-dialog.component';
import { ModifierDialogComponent } from './tab-tables-freelance/factures-tab/modifier-dialog/modifier-dialog.component';
import { TelechargerDialogComponent } from './tab-tables-freelance/factures-tab/telecharger-dialog/telecharger-dialog.component';
import { EditDialogComponent } from './tab-tables-freelance/payments-tab/edit-dialog/edit-dialog.component';
import { DownloadDialogComponent } from './tab-tables-freelance/payments-tab/download-dialog/download-dialog.component';
import {UiService} from "./services/ui.service";

@NgModule({
  declarations: [
    ClientsTableComponent,
    FiTableComponent,
    FmTableComponent,
    PaiementsTableComponent,
    ClientConsulterDialogComponent,
    ClientModifierDialogComponent,
    CliJobsComponent,
    FilterFormComponent,
    ClijobConsulterDialogComponent,
    ClijobFacturerDialogComponent,
    ClijobModifierDialogComponent,
    FiVisualiserDialogComponent,
    FiModifierDialogComponent,
    FmVisualiserDialogComponent,
    FmModifierDialogComponent,
    FmAddDialogComponent,
    PaiementAddDialogComponent,
    PaiementModifierDialogComponent,
    PaiementClotureDialogComponent,
    ClientProfileComponent,
    ContactsComponent,
    FilterFormShortComponent,
    ContactProfileComponent,
    QuotesComponent,
    StatisticsComponent,
    InvoicingMpComponent,
    FreelancersTabComponent,
    FreejobsTabComponent,
    FacturesTabComponent,
    PaymentsTabComponent,
    FilterFormByFreelanceComponent,
    FreelanceConsulterDialogComponent,
    FreelanceFilterFormComponent,
    PayeeDialogComponent,
    ModifierDialogComponent,
    TelechargerDialogComponent,
    EditDialogComponent,
    DownloadDialogComponent
  ],
  imports: [
    CommonModule,
    PoFacturesRoutingModule,
    MatSelectModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MaterialModule,
    EffectsModule.forRoot([ClientTabEffects]),
  ],
  exports: [
    MatButtonModule,
    MatDatepickerModule
  ],
  entryComponents: [
    ClientConsulterDialogComponent,
    ClientModifierDialogComponent,
    ClijobConsulterDialogComponent,
    ClijobFacturerDialogComponent,
    ClijobModifierDialogComponent,
    FiVisualiserDialogComponent,
    FiModifierDialogComponent,
    FmVisualiserDialogComponent,
    FmModifierDialogComponent,
    FmAddDialogComponent,
    PaiementAddDialogComponent,
    PaiementModifierDialogComponent,
    PaiementClotureDialogComponent,
    FreelanceConsulterDialogComponent,
    PayeeDialogComponent,
    ModifierDialogComponent,
    TelechargerDialogComponent,
    EditDialogComponent,
    DownloadDialogComponent
  ],
  providers: [ UiService ]
})
export class PoFacturesModule { }
