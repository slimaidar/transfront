import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Freejob} from '../../../freelances/models/freejob';
import {FreelancerService} from '../../services/freelancer.service';
import {CliJobs} from '../../../project/models/cli-jobs';
import {State} from '../../../reducers/index';
import {Store} from '@ngrx/store';
import {RouteType} from '../../store/client.actions';

@Component({
  selector: 'app-freejobs-tab',
  templateUrl: './freejobs-tab.component.html',
  styleUrls: ['./freejobs-tab.component.css']
})
export class FreejobsTabComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['code_tache', 'intitule_tache', 'service', 'paire_langue', 'domaine', 'volume', 'volume_trite', 'date_debut', 'date_echeance'];
  dataSource = new MatTableDataSource<Freejob>();
  columnsSelect = [{'name': 'col1', 'statu': true}, {'name': 'col2', 'statu': true}];
  oldCols = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private freeService: FreelancerService,
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.getFreeJobs();
    this.store.dispatch(new RouteType('fre'));
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getFreeJobs() {
    this.freeService.getFreeJobs().subscribe(res => {
      this.dataSource.data = <CliJobs[]> res;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns.filter(el => el !== 'date_echeance'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('date_echeance');
  }

}
