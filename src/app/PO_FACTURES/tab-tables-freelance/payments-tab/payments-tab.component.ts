import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {PoFacturesService} from "../../services/po-factures.service";
import {Paiement} from "../../../freelances/models/paiement";
import {State} from "../../../reducers/index";
import {Store} from "@ngrx/store";
import {RouteType} from "../../store/client.actions";
import {EditDialogComponent} from "./edit-dialog/edit-dialog.component";
import {DownloadDialogComponent} from "./download-dialog/download-dialog.component";

@Component({
  selector: 'app-payments-tab',
  templateUrl: './payments-tab.component.html',
  styleUrls: ['./payments-tab.component.scss']
})
export class PaymentsTabComponent implements OnInit {

  displayedColumns = ['no', 'factures', 'date', 'montant', 'freelancer', 'status', 'actions'];
  columnsSelect = [{'name': 'col1', 'statu': true}, {'name': 'col2', 'statu': true}];
  oldCols = [];
  dataSource = new MatTableDataSource<Paiement>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private poService: PoFacturesService,
              private dialog: MatDialog,
              private store: Store<State>) { }

  ngOnInit() {
    this.getPaiements();
    this.store.dispatch(new RouteType('fre'));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private getPaiements() {
    this.poService.getPaiements().subscribe(res => {
        this.dataSource.data = <Paiement[]>res;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns
      .filter(el => el !== 'Actions'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('Actions');
  }

  modifierDialog() {
    this.dialog.open(EditDialogComponent);
  }

  telechargerDialog() {
    this.dialog.open(DownloadDialogComponent);
  }

}
