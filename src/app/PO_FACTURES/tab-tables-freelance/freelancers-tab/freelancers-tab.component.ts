import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../reducers/index';
import {RouteType} from '../../store/client.actions';
import {FreelancerService} from '../../services/freelancer.service';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Freelancer} from '../../../freelances/models/freelancer';
import {FreelanceConsulterDialogComponent} from "./freelance-consulter-dialog/freelance-consulter-dialog.component";

@Component({
  selector: 'app-freelancers-tab',
  templateUrl: './freelancers-tab.component.html',
  styleUrls: ['./freelancers-tab.component.css']
})
export class FreelancersTabComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['fullname', 'filiale', 'ville', 'email', 'tel', 'actions'];
  dataSource = new MatTableDataSource<Freelancer>();
  columnsSelect = [{'name': 'col1', 'statu': true}, {'name': 'col2', 'statu': true}];
  oldCols = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private store: Store<State>,
    private freService: FreelancerService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.getFreelancers();
    this.store.dispatch(new RouteType('fre'));
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns.filter(el => el !== 'actions'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('actions');
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getFreelancers() {
    this.freService.getFreelancers().subscribe(res => {
      this.dataSource.data = <Freelancer[]>res;
    });
  }

  consulterDialog() {
    this.dialog.open(FreelanceConsulterDialogComponent);
  }

}
