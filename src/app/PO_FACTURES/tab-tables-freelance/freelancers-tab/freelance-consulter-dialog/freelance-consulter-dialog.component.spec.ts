import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreelanceConsulterDialogComponent } from './freelance-consulter-dialog.component';

describe('FreelanceConsulterDialogComponent', () => {
  let component: FreelanceConsulterDialogComponent;
  let fixture: ComponentFixture<FreelanceConsulterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreelanceConsulterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreelanceConsulterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
