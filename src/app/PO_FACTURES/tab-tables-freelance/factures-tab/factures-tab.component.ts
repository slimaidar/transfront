import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FreelancerService} from '../../services/freelancer.service';
import {State} from '../../../reducers/index';
import {Store} from '@ngrx/store';
import {RouteType} from '../../store/client.actions';
import {Facture} from "../../../freelances/models/facture";
import {PayeeDialogComponent} from "./payee-dialog/payee-dialog.component";
import {ModifierDialogComponent} from "./modifier-dialog/modifier-dialog.component";
import {TelechargerDialogComponent} from "./telecharger-dialog/telecharger-dialog.component";

@Component({
  selector: 'app-factures-tab',
  templateUrl: './factures-tab.component.html',
  styleUrls: ['./factures-tab.component.css']
})
export class FacturesTabComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['code', 'freejobs', 'date_facturation', 'montant', 'status', 'actions'];
  dataSource = new MatTableDataSource<Facture>();
  columnsSelect = [{'name': 'col1', 'statu': true}, {'name': 'col2', 'statu': true}];
  oldCols = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private freeService: FreelancerService,
    private store: Store<State>,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getFactures()
    this.store.dispatch(new RouteType('fre'));
  }

  getFactures() {
    this.freeService.getFactures().subscribe(
      res => {
        this.dataSource.data = <Facture[]>res;
      }
    )
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns.filter(el => el !== 'actions'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('actions');
  }

  payeeDialog() {
    this.dialog.open(PayeeDialogComponent);
  }

  modifierDialog() {
    this.dialog.open(ModifierDialogComponent);
  }

  telechargerDialog() {
    this.dialog.open(TelechargerDialogComponent);
  }
}
