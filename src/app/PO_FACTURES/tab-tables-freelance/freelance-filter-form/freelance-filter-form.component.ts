import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-freelance-filter-form',
  templateUrl: './freelance-filter-form.component.html',
  styleUrls: ['./freelance-filter-form.component.scss']
})
export class FreelanceFilterFormComponent implements OnInit {

  filterForm = new FormGroup({
    project: new FormControl(),
    dateDebut: new FormControl(),
    dateFin: new FormControl()
  });

  constructor() { }

  ngOnInit() {
  }

  selectChange() {
      console.log(this.filterForm.value);
  }
}
