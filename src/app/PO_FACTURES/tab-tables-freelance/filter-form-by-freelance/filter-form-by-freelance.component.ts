import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-filter-form-by-freelance',
  templateUrl: './filter-form-by-freelance.component.html',
  styleUrls: ['./filter-form-by-freelance.component.scss']
})
export class FilterFormByFreelanceComponent implements OnInit {

  typeFreelance = '';
  constructor() { }

  ngOnInit() {
  }

  filterValueChanged() {
    console.log(this.typeFreelance);
  }

}
