import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClientsTableComponent} from './tab-tables/clients-table/clients-table.component';
import {FiTableComponent} from './tab-tables/fi-table/fi-table.component';
import {FmTableComponent} from './tab-tables/fm-table/fm-table.component';
import {PaiementsTableComponent} from './tab-tables/paiements-table/paiements-table.component';
import {PoFactureComponent} from './po-facture/po-facture.component';
import {CliJobsComponent} from './tab-tables/cli-jobs/cli-jobs.component';
import {ContactsComponent} from './tab-tables/contacts/contacts.component';
import {QuotesComponent} from './tab-tables/quotes/quotes.component';
import {StatisticsComponent} from './tab-tables/statistics/statistics.component';
import {InvoicingMpComponent} from './tab-tables/invoicing-mp/invoicing-mp.component';
import {FreelancersTabComponent} from './tab-tables-freelance/freelancers-tab/freelancers-tab.component';
import {PaymentsTabComponent} from './tab-tables-freelance/payments-tab/payments-tab.component';
import {FacturesTabComponent} from './tab-tables-freelance/factures-tab/factures-tab.component';
import {FreejobsTabComponent} from './tab-tables-freelance/freejobs-tab/freejobs-tab.component';

const routes: Routes = [
  { path: 'po&facture',
    component: PoFactureComponent,
    children: [
      { path: 'client/clients', component: ClientsTableComponent},
      { path: 'client/contacts', component: ContactsComponent},
      { path: 'client/facture_indeveduelles', component: FiTableComponent},
      { path: 'client/clijobs', component: CliJobsComponent},
      { path: 'client/quotes', component: QuotesComponent},
      { path: 'client/statistics', component: StatisticsComponent},
      { path: 'client/invoicing', component: InvoicingMpComponent},
      { path: 'client/facture_mensuelles', component: FmTableComponent},
      { path: 'client/paiements', component: PaiementsTableComponent},

      { path: 'freelance/freelancer', component: FreelancersTabComponent},
      { path: 'freelance/freejobs', component: FreejobsTabComponent},
      { path: 'freelance/factures', component: FacturesTabComponent},
      { path: 'freelance/paiements', component: PaymentsTabComponent},
      { path: '', redirectTo: 'client/clients', pathMatch: 'prefix' }
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoFacturesRoutingModule { }
