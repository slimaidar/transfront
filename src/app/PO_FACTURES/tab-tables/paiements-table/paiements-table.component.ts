import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { State } from './../../../reducers/index';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { PoFacturesService } from '../../services/po-factures.service';
import { FactureMensuelle } from '../../../invoice/models/facture-mensuelle';
import { PaiementClotureDialogComponent } from './paiement-cloture-dialog/paiement-cloture-dialog.component';
import { PaiementModifierDialogComponent } from './paiement-modifier-dialog/paiement-modifier-dialog.component';
import { PaiementAddDialogComponent } from './paiement-add-dialog/paiement-add-dialog.component';
import { Paiment } from "../../../invoice/models/paiment";

@Component({
  selector: 'app-paiements-table',
  templateUrl: './paiements-table.component.html',
  styleUrls: ['./paiements-table.component.scss']
})
export class PaiementsTableComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['no', 'factures', 'client', 'date', 'montant', 'status', 'actions'];
  dataSource = new MatTableDataSource<Paiment>();
  columnsSelect = [{ 'name': 'col1', 'statu': true }, { 'name': 'col2', 'statu': true }];
  oldCols = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private poService: PoFacturesService,
    private dialog: MatDialog,
    private store: Store<State>,
    private router: Router) {
    store.pipe(
      select(state => state.clients)
    ).subscribe(
      res => {
        if (!res.selected) {
          router.navigateByUrl('/po&facture/client/clients')
        }
      } )
  }

  ngOnInit() {
    this.poService.getPaiements().subscribe(res => {
      this.dataSource.data = <Paiment[]>res;
    });
  }

  getPaiements() {
    this.poService.getPaiements().subscribe(res => {
      this.dataSource.data = <Paiment[]>res;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns.filter(el => el !== 'actions'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('actions');
  }

  cloturerDialog() {
    this.dialog.open(PaiementClotureDialogComponent);
  }

  modifierDialog() {
    this.dialog.open(PaiementModifierDialogComponent);
  }

  addPaiement() {
    this.dialog.open(PaiementAddDialogComponent);
  }

}
