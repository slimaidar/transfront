import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementAddDialogComponent } from './paiement-add-dialog.component';

describe('PaiementAddDialogComponent', () => {
  let component: PaiementAddDialogComponent;
  let fixture: ComponentFixture<PaiementAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
