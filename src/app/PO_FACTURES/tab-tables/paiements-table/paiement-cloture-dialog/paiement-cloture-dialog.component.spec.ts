import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementClotureDialogComponent } from './paiement-cloture-dialog.component';

describe('PaiementClotureDialogComponent', () => {
  let component: PaiementClotureDialogComponent;
  let fixture: ComponentFixture<PaiementClotureDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementClotureDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementClotureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
