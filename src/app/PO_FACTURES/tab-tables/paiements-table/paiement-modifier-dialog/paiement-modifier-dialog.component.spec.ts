import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementModifierDialogComponent } from './paiement-modifier-dialog.component';

describe('PaiementModifierDialogComponent', () => {
  let component: PaiementModifierDialogComponent;
  let fixture: ComponentFixture<PaiementModifierDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaiementModifierDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementModifierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
