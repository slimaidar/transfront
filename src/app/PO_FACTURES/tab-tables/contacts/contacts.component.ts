import { Store, select } from '@ngrx/store';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {PoFacturesService} from '../../services/po-factures.service';
import {Contact} from '../../../invoice/models/contact';
import { State } from 'src/app/reducers';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  displayedColumns = ['name', 'email', 'phone', 'fax', 'account'];
  columnsSelect = [{'name': 'col1', 'statu': true}, {'name': 'col2', 'statu': true}];
  oldCols = [];
  dataSource = new MatTableDataSource<Contact>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  editing = false;
  idContant: string;
  idClient: number;
  constructor(
    private poService: PoFacturesService,
    private store: Store<State>,
    private router: Router) {
    store.pipe(
      select(state => state.clients)
    ).subscribe(
      res => {
        if (!res.selected) {
          router.navigateByUrl('/po&facture/client/clients')
        } else {
          this.idClient = res.clientId;
        }
      }
    )
  }


  ngOnInit() {
    this.getContacts();
  }

  getContacts() {
    this.poService.getContacts(this.idClient).subscribe(
      res => {
          this.dataSource.data = <Contact[]>res;
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  editContact(idContact) {
    this.editing = this.editing ? false : true;
    this.idContant = idContact;
    this.displayedColumns = this.editing ? ['name'] : ['name', 'position', 'email', 'phone'];
  }
}
