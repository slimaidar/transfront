import {Component, Input, OnInit} from '@angular/core';
import {PoFacturesService} from '../../../services/po-factures.service';
import {Contact} from '../../../../invoice/models/contact';

@Component({
  selector: 'app-contact-profile',
  templateUrl: './contact-profile.component.html',
  styleUrls: ['./contact-profile.component.scss']
})
export class ContactProfileComponent implements OnInit {

  @Input() contactId;
  contact: Contact;

  constructor(private poService: PoFacturesService) { }

  ngOnInit() {
    this.poService.getContact( parseInt(this.contactId) ).subscribe(
      res => {
        this.contact = <Contact>res;
      }
    );
  }

  deleteContact() {
    console.log('button working')
  }
}
