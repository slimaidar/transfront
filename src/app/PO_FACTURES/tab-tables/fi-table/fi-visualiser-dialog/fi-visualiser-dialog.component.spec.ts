import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiVisualiserDialogComponent } from './fi-visualiser-dialog.component';

describe('FiVisualiserDialogComponent', () => {
  let component: FiVisualiserDialogComponent;
  let fixture: ComponentFixture<FiVisualiserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiVisualiserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiVisualiserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
