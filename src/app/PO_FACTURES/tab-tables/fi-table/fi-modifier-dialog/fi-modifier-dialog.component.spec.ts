import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiModifierDialogComponent } from './fi-modifier-dialog.component';

describe('FiModifierDialogComponent', () => {
  let component: FiModifierDialogComponent;
  let fixture: ComponentFixture<FiModifierDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiModifierDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiModifierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
