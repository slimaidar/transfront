import { Router } from '@angular/router';
import { State } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FactureIndividuelle } from '../../../invoice/models/facture-individuelle';
import { PoFacturesService } from '../../services/po-factures.service';
import { FiVisualiserDialogComponent } from './fi-visualiser-dialog/fi-visualiser-dialog.component';
import { FiModifierDialogComponent } from './fi-modifier-dialog/fi-modifier-dialog.component';

@Component({
  selector: 'app-fi-table',
  templateUrl: './fi-table.component.html',
  styleUrls: ['./fi-table.component.scss']
})
export class FiTableComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['no', 'clijob', 'montant', 'date', 'status', 'Actions'];
  dataSource = new MatTableDataSource<FactureIndividuelle>();
  columnsSelect = [{ 'name': 'col1', 'statu': true }, { 'name': 'col2', 'statu': true }];
  oldCols = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private poService: PoFacturesService,
    private dialog: MatDialog,
    private store: Store<State>,
    private router: Router) {
    store.pipe(
      select(state => state.clients)
    ).subscribe(
      res => {
        if (!res.selected) {
          router.navigateByUrl('/po&facture/client/clients')
        }
      }
    )
  }

  ngOnInit() {
    this.getFi();
  }

  getFi() {
    this.poService.getFi().subscribe(res => {
      this.dataSource.data = <FactureIndividuelle[]>res;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns.filter(el => el !== 'Actions'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('Actions');
  }

  visualiserDialog() {
    this.dialog.open(FiVisualiserDialogComponent);
  }

  modifierDialog() {
    this.dialog.open(FiModifierDialogComponent);
  }
}
