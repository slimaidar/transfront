import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Client} from "../../../clients/models/client";

@Component({
  selector: 'app-filter-form',
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.scss']
})
export class FilterFormComponent implements OnInit {
  apiUrl = environment.apiUrl
  constructor(
    private http: HttpClient
  ) { }

  form = {
    'filiale': '',
    'compte': '',
    'projet': '',
    'dateDebut': '',
    'dateFin': '',
  };
  filiales = ['trans4orient', 'trans4europe'];
  comptes = [];
  projects = [];
  @Output() public clientEvent = new EventEmitter<Client>();
  ngOnInit() {
  }

  filterValueChanged(name, e) {
    this.form[name] = e;
    this.http.get(`${this.apiUrl}/api/clients/?branch=${this.form.filiale}&account=${this.form.compte}&projects=${this.form.projet}&starts_at=${this.form.dateDebut}&ends_at=${this.form.dateFin}`)
      .subscribe(res => {
        this.clientEvent.emit(<Client>res);
      })
  }

  cleanFilter() {
    this.http.get(`${this.apiUrl}/api/clients`)
      .subscribe(res => {
        this.clientEvent.emit(<Client>res);
    })
  }

  getFialiales() {

  }

  getComptes() {

  }

  getProjects() {

  }
}
