import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmTableComponent } from './fm-table.component';

describe('FmTableComponent', () => {
  let component: FmTableComponent;
  let fixture: ComponentFixture<FmTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
