import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmAddDialogComponent } from './fm-add-dialog.component';

describe('FmAddDialogComponent', () => {
  let component: FmAddDialogComponent;
  let fixture: ComponentFixture<FmAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
