import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmModifierDialogComponent } from './fm-modifier-dialog.component';

describe('FmModifierDialogComponent', () => {
  let component: FmModifierDialogComponent;
  let fixture: ComponentFixture<FmModifierDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmModifierDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmModifierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
