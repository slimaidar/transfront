import { Router } from '@angular/router';
import { State } from 'src/app/reducers';
import { Store, select } from '@ngrx/store';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FactureMensuelle } from '../../../invoice/models/facture-mensuelle';
import { PoFacturesService } from "../../services/po-factures.service";
import { FmVisualiserDialogComponent } from "./fm-visualiser-dialog/fm-visualiser-dialog.component";
import { FmModifierDialogComponent } from "./fm-modifier-dialog/fm-modifier-dialog.component";
import { FmAddDialogComponent } from "./fm-add-dialog/fm-add-dialog.component";


@Component({
  selector: 'app-fm-table',
  templateUrl: './fm-table.component.html',
  styleUrls: ['./fm-table.component.scss']
})
export class FmTableComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['no', 'fi', 'date', 'montant', 'actions'];
  dataSource = new MatTableDataSource<FactureMensuelle>();
  columnsSelect = [{ 'name': 'col1', 'statu': true }, { 'name': 'col2', 'statu': true }];
  oldCols = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private poService: PoFacturesService,
    private dialog: MatDialog,
    private store: Store<State>,
    private router: Router) {
    store.pipe(
      select(state => state.clients)
    ).subscribe(
      res => {
        if (!res.selected) {
          router.navigateByUrl('/po&facture/client/clients')
        }
      })
  }

  ngOnInit() {
    this.poService.getFm().subscribe(res => {
      this.dataSource.data = <FactureMensuelle[]>res;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns.filter(el => el !== 'actions'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('actions');
  }

  visualiserDialog() {
    this.dialog.open(FmVisualiserDialogComponent);
  }

  modifierDialog() {
    this.dialog.open(FmModifierDialogComponent);
  }

  addFm() {
    this.dialog.open(FmAddDialogComponent);
  }
}
