import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmVisualiserDialogComponent } from './fm-visualiser-dialog.component';

describe('FmVisualiserDialogComponent', () => {
  let component: FmVisualiserDialogComponent;
  let fixture: ComponentFixture<FmVisualiserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmVisualiserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmVisualiserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
