import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filter-form-short',
  templateUrl: './filter-form-short.component.html',
  styleUrls: ['./filter-form-short.component.scss']
})
export class FilterFormShortComponent implements OnInit {

  form = {
    'filiale': '',
    'compte': '',
  };
  constructor() { }

  ngOnInit() {
  }

  filterValueChanged(name, e) {
    console.log(name + ' ' + e);
  }
}
