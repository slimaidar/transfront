import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Client} from '../../../../clients/models/client';
import {PoFacturesService} from '../../../services/po-factures.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {RemoveClientId} from '../../../store/client.actions';
import {select, Store} from '@ngrx/store';
import {State} from '../../../../reducers/index';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.scss']
})
export class ClientProfileComponent implements OnInit {

  profileForm = new FormGroup({
    name: new FormControl(''),
    adresse: new FormGroup({
      adresse1: new FormControl(''),
      adresse2: new FormControl(''),
    }),
    city: new FormControl(''),
    zip: new FormControl(''),
    state: new FormControl(''),
    webSite: new FormControl(''),
    vatNumber: new FormControl(''),
    marketingLink: new FormControl(''),
    minfee: new FormControl(''),
    projectManager: new FormControl(''),
    contactInfo: new FormGroup({
      email: new FormControl(''),
      phone: new FormControl(''),
      fax: new FormControl('')
    }),
    cats : new FormControl(''),
    branch: new FormControl(''),
    branches: new FormControl(''),
    country: new FormControl(''),
    currencies: new FormControl('')
  });
  isEditing = false;
  client: Client;

  branches: Object;
  currencies: Object;
  countries: Object;

  @Output() public closeProfile = new EventEmitter();
  selectedOptions = {
    'branch': '',
  };

  constructor(
    private poService: PoFacturesService,
    private router: Router,
    private store: Store<State>
  ) {
    store.pipe(
      select( state => state.clients.clientId )
    )
    .subscribe(res => {
      this.getClient(+res);
    });
  }

  ngOnInit() {
    if ( !localStorage.hasOwnProperty('clientId') ) {
      this.router.navigateByUrl('/po&facture/client/clients');
    }
    this.getBranches(+localStorage.hasOwnProperty('clientId'));
    this.getCurrencies();
    this.getCountries();
  }

  getClient(id: number): void {
    if ( id !== 0 ) {
      this.poService.getClient( id ).subscribe(
        res => {
          this.client = <Client>res;
          this.selectedOptions.branch = this.client['BRANCH']['BRANCH'];
        }
      );
    }
  }

  updateClient(): void {
    console.log('yo');
  }

  getBranches(id): void {
    this.poService.getBranches().subscribe(res => {
      this.branches = res;
    });
  }

  getCurrencies(): void {
    this.poService.getCurrencies().subscribe(res => {
      this.currencies = res;
    });
  }

  getCountries(): void {
    this.poService.getCountries().subscribe(res => {
      this.countries = res;
    });
  }

  closeProfileEvent() {
    this.closeProfile.emit(false);
  }

  editClient() {
    console.log(this.profileForm)
  }
}
