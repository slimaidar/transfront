import { Component, OnInit, ViewChild } from '@angular/core';
import { PoFacturesService } from '../../services/po-factures.service';
import { Client } from '../../../clients/models/client';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ClientConsulterDialogComponent } from './client-consulter-dialog/client-consulter-dialog.component';
import { ClientModifierDialogComponent } from './client-modifier-dialog/client-modifier-dialog.component';
import { Store, select } from '@ngrx/store';
import { RemoveClientId, RouteType, SaveClientId } from '../../store/client.actions';
import { State } from '../../../reducers/index';
import { LoadClientTabs } from '../../store/tab-tables/client-tab.actions';

@Component({
  selector: 'app-clients-table',
  templateUrl: './clients-table.component.html',
  styles: [
    'table {width: 100%; overflow: scroll;}',
    'th { font-weight: bold; background: #3F51B5; color: #FFFFFF; font-size: .8em;}',
    '.nav-container { width: 90%; margin: auto }',
    '.selected { background: #E8EAF6 }',
    'tr:hover{ background-color: #F5F5F5}'
  ]
})
export class ClientsTableComponent implements OnInit {

  displayedColumns = ['name', 'currency', 'email', 'phone', 'country'];
  columnsSelect = [{ 'name': 'col1', 'statu': true }, { 'name': 'col2', 'statu': true }];
  oldCols = [];
  dataSource = new MatTableDataSource<Client>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  showProfile = false;
  clientId: number;
  isLoading = true;

  constructor(private poService: PoFacturesService,
    private dialog: MatDialog,
    private store: Store<State>) { }

  ngOnInit() {
    this.getClients();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    localStorage.removeItem('clientId');
    this.store.dispatch(new RouteType('cli'));
  }

  consulterDialog() {
    this.dialog.open(ClientConsulterDialogComponent);
  }

  modifierDialog() {
    this.dialog.open(ClientModifierDialogComponent);
  }

  getClients() {
    this.store.pipe(
      select(state => state.clientTab)
    ).subscribe(
      resState => {
        if (!resState.loaded) {
          this.poService.getClients().subscribe(res => {
            this.store.dispatch(new LoadClientTabs(<Client[]>res, true))
            this.isLoading = false;
          });
        } else {
          this.dataSource.data = resState.clients;
          this.isLoading = false;
        }

      }
    )
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns
      .filter(el => el !== 'country'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('country');
  }

  unselectClient() {
    this.displayedColumns = ['name', 'currency', 'email', 'phone', 'country'];
    this.showProfile = false;
    this.store.dispatch(new RemoveClientId());
    this.clientId = null;
  }

  selectClient(code: number) {
    this.displayedColumns = ['name']
    this.showProfile = true;
    this.clientId = code;
    this.store.dispatch(new SaveClientId(code));
  }

  filterTable(event) {
    this.dataSource.data = event
  }
}
