import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientConsulterDialogComponent } from './client-consulter-dialog.component';

describe('ClientConsulterDialogComponent', () => {
  let component: ClientConsulterDialogComponent;
  let fixture: ComponentFixture<ClientConsulterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientConsulterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientConsulterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
