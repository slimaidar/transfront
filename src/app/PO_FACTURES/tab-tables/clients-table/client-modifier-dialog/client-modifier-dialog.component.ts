import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client-modifier-dialog',
  templateUrl: './client-modifier-dialog.component.html',
  styleUrls: ['./client-modifier-dialog.component.scss']
})
export class ClientModifierDialogComponent implements OnInit {

  branches = ['trans4orient', 'trans4europe'];
  devises = ['USD', 'MAD', 'EUR'];
  jours = [1, 2, 3, 4, 5, 10, 15, 20];
  constructor() { }

  ngOnInit() {
  }

}
