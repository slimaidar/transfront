import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientModifierDialogComponent } from './client-modifier-dialog.component';

describe('ClientModifierDialogComponent', () => {
  let component: ClientModifierDialogComponent;
  let fixture: ComponentFixture<ClientModifierDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientModifierDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientModifierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
