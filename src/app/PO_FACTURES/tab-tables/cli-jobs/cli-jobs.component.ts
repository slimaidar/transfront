import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { PoFacturesService } from '../../services/po-factures.service';
import { CliJobs } from '../../../project/models/cli-jobs';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ClijobConsulterDialogComponent } from './clijob-consulter-dialog/clijob-consulter-dialog.component';
import { ClijobFacturerDialogComponent } from './clijob-facturer-dialog/clijob-facturer-dialog.component';
import { ClijobModifierDialogComponent } from './clijob-modifier-dialog/clijob-modifier-dialog.component';
import { State } from "../../../reducers/index";
import { Store, select } from "@ngrx/store";
import { RouteType } from "../../store/client.actions";
import { Router } from '@angular/router';

@Component({
  selector: 'app-cli-jobs',
  templateUrl: './cli-jobs.component.html',
  styleUrls: ['./cli-jobs.component.scss']
})
export class CliJobsComponent implements OnInit, AfterViewInit, AfterViewInit {

  displayedColumns: string[] = ['code', 'intitule', 'service', 'language_paire', 'status', 'prix', 'tarif', 'volume', 'date_echeance', 'date_creation', 'Actions'];
  dataSource = new MatTableDataSource<CliJobs>();
  columnsSelect = [{ 'name': 'col1', 'statu': true }, { 'name': 'col2', 'statu': true }];
  oldCols = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  clientid: number;

  constructor(private poService: PoFacturesService,
    private dialog: MatDialog,
    private store: Store<State>,
    private router: Router) {
    store.pipe(
      select(state => state.clients)
    ).subscribe(
      res => {
        if (!res.selected) {
          router.navigateByUrl('/po&facture/client/clients');
        } else {
          this.clientid = res.clientId
        }
      }
    )
  }

  ngOnInit() {
    this.getCliJobs();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getCliJobs() {
    this.poService.getCliJobs(this.clientid).subscribe(res => {
      this.dataSource.data = <CliJobs[]>res;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  selectCols(select) {
    this.displayedColumns = this.displayedColumns.filter(el => el !== 'Actions'); // remove action col and add it later so it can be the last one

    this.oldCols.forEach(el => { // remove old cols
      this.displayedColumns = this.displayedColumns.filter(col => col !== el);
    });

    select.value.forEach((col) => {
      this.displayedColumns.push(col);
      this.oldCols.push(col); // to remember added columns so u can delete them khfen zerben
    });

    this.displayedColumns = this.displayedColumns.filter((el, i, a) => i === a.indexOf(el));
    this.displayedColumns.push('Actions');
  }

  //  Dialogs
  consulterDialog() {
    this.dialog.open(ClijobConsulterDialogComponent);
  }
  facturerDialog() {
    this.dialog.open(ClijobFacturerDialogComponent);
  }
  modifierDialog() {
    this.dialog.open(ClijobModifierDialogComponent);
  }

}
