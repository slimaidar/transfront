import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material';
import {PlanningDialogComponent} from './dialogs/planning-dialog/planning-dialog.component';
import {CreateEntretienDialogComponent} from './dialogs/create-entretien-dialog/create-entretien-dialog.component';
import {ListEntretienDialogComponent} from './dialogs/list-entretien-dialog/list-entretien-dialog.component';
import {ListEvaluationDialogComponent} from './dialogs/list-evaluation-dialog/list-evaluation-dialog.component';
import {ListProjectDialogComponent} from './dialogs/list-project-dialog/list-project-dialog.component';
import {ListRemarqueDialogComponent} from './dialogs/list-remarque-dialog/list-remarque-dialog.component';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  language = 'ENFR';
  employee = {
    id: 1,
    name: 'Jhon doe',
    statistiquesENFR: {
      p_global: {
        tradiction: '430 mots/h',
        postEdition: '760 mots/h',
        relecture: '1430 mots/h',
        qa: '3650 mots/h'
      },
      p_30: {
        tradiction: '422 mots/h',
        postEdition: '730 mots/h',
        relecture: '2430 mots/h',
        qa: '2650 mots/h'
      },
      note_g: {
        tradiction: '422 mots/h',
        postEdition: '730 mots/h',
      },
      note_30: {
        tradiction: '422 mots/h',
        postEdition: '730 mots/h',
      }
    },
    statistiquesESFR: {
      p_global: {
        tradiction: '230 mots/h',
        postEdition: '360 mots/h',
        relecture: '930 mots/h',
        qa: '3612 mots/h'
      },
      p_30: {
        tradiction: '444 mots/h',
        postEdition: '750 mots/h',
        relecture: '1430 mots/h',
        qa: '3650 mots/h'
      },
      note_g: {
        tradiction: '122 mots/h',
        postEdition: '530 mots/h',
      },
      note_30: {
        tradiction: '322 mots/h',
        postEdition: '930 mots/h',
      }
    }
  };
  statistics = this.employee.statistiquesENFR;

    constructor(private http: HttpClient, private planningDialog: MatDialog) { }

  changeStatisticLang(lang: string) {
    if (lang === 'ENFR') {
      this.statistics = this.employee.statistiquesENFR;
    }

    if (lang === 'ESFR') {
      this.statistics = this.employee.statistiquesESFR;
    }
  }

  // open dialogs

  openPlanningDialog() {
    const planningDialog = this.planningDialog.open(PlanningDialogComponent, {
      data: this.employee.id
    });

    planningDialog.afterClosed().subscribe(data => {
      console.log('dialog closed');
    });
  }

  public openCreatePlanningDialog() {
    const dialog = this.planningDialog.open(CreateEntretienDialogComponent, {
      data: this.employee.id
    });

    dialog.afterClosed().subscribe(data => {
      console.log('dialog closed');
    });
  }

  openListEntretientDialog() {
    const dialog = this.planningDialog.open(ListEntretienDialogComponent, {
      data: this.employee.id
    });

    dialog.afterClosed().subscribe(data => {
      console.log('dialog closed');
    });
  }

  openListEvaluationDialog() {
    const dialog = this.planningDialog.open(ListEvaluationDialogComponent, {
      data: this.employee.id
    });

    dialog.afterClosed().subscribe(data => {
      console.log('dialog closed');
    });
  }

  openListProjectDialog() {
    const dialog = this.planningDialog.open(ListProjectDialogComponent, {
      data: this.employee.id
    });

    dialog.afterClosed().subscribe(data => {
      console.log('dialog closed');
    });
  }

  openListRemarquetDialog() {
    const dialog = this.planningDialog.open(ListRemarqueDialogComponent, {
      data: this.employee.id
    });

    dialog.afterClosed().subscribe(data => {
      console.log('dialog closed');
    });
  }
  // end open dialogs

  async downloadCV(id: number): Promise<Blob> {
    const file =  await this.http.get<Blob>(
      'put url here yo!',
      {responseType: 'blob' as 'json'}).toPromise();
    return file;
  }

  captureScreen() {
    const restorPage = document.body.innerHTML;
    const printContent = document.querySelector('#content').innerHTML;
    document.body.innerHTML = printContent;
    window.print();
    // document.body.innerHTML = restorPage;
    window.location.reload();
  }

  ngOnInit() {
  }

}
