import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-list-project-dialog',
  template: `
    <h4><b class="text-primary">Tout les Projets de: Jhon doe</b></h4>
    <p>22/09/18 18h30 – Phonak – TRA 430 mots.</p> <hr>
    <p>22/09/18 17h – Siemens AG – REL 6600 mots.</p> <hr>
    <p>21/09/18 15h – AWS – POST 15500 mots.</p> <hr>
    <p>04/09/18 – Relecture 85,3.</p> <hr>
    <mat-dialog-actions>
      <button [mat-dialog-close]="true" mat-button color="warn" >Close</button>
    </mat-dialog-actions>
  `,
  styles: [
    'hr {opacity: 0.2;}',
    'h4 {margin-bottom: 1em}',
    'p {font-size: .8em}'
  ]
})
export class ListProjectDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
