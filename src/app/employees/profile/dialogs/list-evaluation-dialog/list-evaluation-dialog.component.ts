import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-list-evaluation-dialog',
  template: `
    <h4><b class="text-primary">Tout les evaluations de: Jhon doe</b></h4>
    <p>15/09/18 – QA 74,5.</p> <hr>
    <p>07/09/18 – Relecture 90,4 .</p> <hr>
    <p>04/09/18 – Relecture 85,3.</p> <hr>
    <p>15/09/18 – QA 74,5.</p> <hr>
    <p>07/09/18 – Relecture 90,4 .</p> <hr>
    <p>04/09/18 – Relecture 85,3.</p> <hr>
    <mat-dialog-actions>
      <button [mat-dialog-close]="true" mat-button color="warn" >Close</button>
    </mat-dialog-actions>
  `,
  styles: [
    'hr {opacity: 0.2;}',
    'h4 {margin-bottom: 1em}',
    'p {font-size: .8em}'
  ]
})
export class ListEvaluationDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
