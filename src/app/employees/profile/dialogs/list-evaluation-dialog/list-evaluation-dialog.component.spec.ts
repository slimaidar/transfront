import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEvaluationDialogComponent } from './list-evaluation-dialog.component';

describe('ListEvaluationDialogComponent', () => {
  let component: ListEvaluationDialogComponent;
  let fixture: ComponentFixture<ListEvaluationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEvaluationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEvaluationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
