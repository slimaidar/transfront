import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRemarqueDialogComponent } from './list-remarque-dialog.component';

describe('ListRemarqueDialogComponent', () => {
  let component: ListRemarqueDialogComponent;
  let fixture: ComponentFixture<ListRemarqueDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRemarqueDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRemarqueDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
