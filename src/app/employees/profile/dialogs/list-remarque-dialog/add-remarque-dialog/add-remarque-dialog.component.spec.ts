import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRemarqueDialogComponent } from './add-remarque-dialog.component';

describe('AddRemarqueDialogComponent', () => {
  let component: AddRemarqueDialogComponent;
  let fixture: ComponentFixture<AddRemarqueDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRemarqueDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRemarqueDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
