import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-remarque-dialog',
  template: `
    <p>
      this will have a form to add [remarque]
    </p>
    <mat-dialog-actions>
      <button mat-flat-button color="primary">Ajouter</button>
      <button mat-flat-button color="warn">annuler</button>
    </mat-dialog-actions>
  `,
  styles: [
    'button{ margin-right: 5px}'
  ]
})
export class AddRemarqueDialogComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
