import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import {ConfirmDeleteProjectDialogComponent} from './confirm-delete-remarque-dialog/confirm-delete-project-dialog.component';
import {AddRemarqueDialogComponent} from './add-remarque-dialog/add-remarque-dialog.component';

@Component({
  selector: 'app-list-remarque-dialog',
  template: `
    <h4><b class="text-primary">Tout Remarques générales de: Jhon doe</b></h4>
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae, non.
      <button mat-icon-button color="warn" (click)="deleteRemarque(1)">
        <mat-icon aria-label="Supprimer cette remarque">delete_forever</mat-icon>
      </button>
    </p> <hr>
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae, non.
      <button mat-icon-button color="warn">
        <mat-icon aria-label="Supprimer cette remarque ">delete_forever</mat-icon>
      </button>
    </p> <hr>
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae, non.
      <button mat-icon-button color="warn">
        <mat-icon aria-label="Supprimer cette remarque ">delete_forever</mat-icon>
      </button>
    </p> <hr>
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae, non.
      <button mat-icon-button color="warn">
        <mat-icon aria-label="Supprimer cette remarque ">delete_forever</mat-icon>
      </button>
    </p> <hr>
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae, non.
      <button mat-icon-button color="warn">
        <mat-icon aria-label="Supprimer cette remarque ">delete_forever</mat-icon>
      </button>
    </p> <hr>
    <mat-dialog-actions>
      <button [mat-dialog-close]="true" mat-button color="warn" >Close</button>
      <button  mat-button color="primary" (click)="AddRemarque()" >Ajouter un remarque</button>
    </mat-dialog-actions>
  `,
  styles: [
    'hr {opacity: 0.2;}',
    'h4 {margin-bottom: 1em}',
    'p {font-size: .8em}'
  ]
})
export class ListRemarqueDialogComponent implements OnInit {

  constructor(private dialogConfirm: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

  }

  deleteRemarque(remarqueId) {
      const confirmDelete = this.dialogConfirm.open(ConfirmDeleteProjectDialogComponent, {
        data: remarqueId
      });
  }

  AddRemarque() {
    const addR = this.dialogConfirm.open(AddRemarqueDialogComponent);

    addR.afterClosed().subscribe(data => {
      console.log('list remarque dialog => AddRemarque()');
    });
  }

}
