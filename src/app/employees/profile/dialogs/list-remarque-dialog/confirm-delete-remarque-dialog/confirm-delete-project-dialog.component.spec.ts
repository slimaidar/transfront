import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDeleteProjectDialogComponent } from './confirm-delete-project-dialog.component';

describe('ConfirmDeleteProjectDialogComponent', () => {
  let component: ConfirmDeleteProjectDialogComponent;
  let fixture: ComponentFixture<ConfirmDeleteProjectDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDeleteProjectDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDeleteProjectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
