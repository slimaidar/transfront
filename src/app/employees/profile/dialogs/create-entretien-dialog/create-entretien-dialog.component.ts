import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-create-entretien-dialog',
  template: `
    <p>
      This dialog will have a form to add [entretien]
    </p>
  `,
  styles: []
})
export class CreateEntretienDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
