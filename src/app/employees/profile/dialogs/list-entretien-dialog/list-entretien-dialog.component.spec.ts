import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEntretienDialogComponent } from './list-entretien-dialog.component';

describe('ListEntretienDialogComponent', () => {
  let component: ListEntretienDialogComponent;
  let fixture: ComponentFixture<ListEntretienDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListEntretienDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEntretienDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
