import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-list-entretien-dialog',
  template: `
    <h4><b class="text-primary">Tout les entretients de: Jhon doe</b></h4>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <p><b>Date: </b>21/07/18 – Guillaume D.</p> <hr>
    <mat-dialog-actions>
      <button [mat-dialog-close]="true" mat-button color="warn" >Close</button>
    </mat-dialog-actions>
  `,
  styles: [
    'hr {opacity: 0.2;}',
    'h4 {margin-bottom: 1em}',
    'p {font-size: .8em}'
  ]
})
export class ListEntretienDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
