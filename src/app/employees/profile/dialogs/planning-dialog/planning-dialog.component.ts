import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-planning-dialog',
  template: `
    <p>
      Here will show the emp planning!
    </p>
  `,
  styles: []
})
export class PlanningDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
