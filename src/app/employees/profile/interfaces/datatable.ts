export interface Productivite {
  traduction: string;
  postEdution: string;
  relecture?: string;
  qa?: string;
}

export interface Statistics {
  ProductiviteGlobale: Productivite;
  Productivite30: Productivite;
  noteGlobal: Productivite;
  note30: Productivite;
}
