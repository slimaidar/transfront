import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesRoutingModule } from './employees-routing.module';
import {EmployeesComponent} from './employees.component';
import {ProfileComponent} from './profile/profile.component';
import {FicheEntretientComponent} from './fiche-entretient/fiche-entretient.component';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    EmployeesRoutingModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [
    EmployeesComponent,
    ProfileComponent,
    FicheEntretientComponent
  ]
})
export class EmployeesModule { }
