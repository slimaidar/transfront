import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeesComponent} from './employees.component';
import {ProfileComponent} from './profile/profile.component';
import {FicheEntretientComponent} from './fiche-entretient/fiche-entretient.component';

const routes: Routes = [
  { path: 'employee', component: EmployeesComponent },
  { path: 'employee/profile/:id', component: ProfileComponent },
  { path: 'employee/fiche/:id', component: FicheEntretientComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
