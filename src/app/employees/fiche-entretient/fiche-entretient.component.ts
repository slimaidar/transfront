import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fiche-entretient',
  templateUrl: './fiche-entretient.component.html',
  styleUrls: ['./fiche-entretient.component.scss']
})
export class FicheEntretientComponent implements OnInit {

  table1Info: any;
  table2Info: Array<Object> = [];

  dataExists = false;

  constructor() {
    if ( !this.dataExists ) {
      this.table1Info = {
        global: {
          observations: '',
          action: '',
        },
        last30: {
          observations: '',
          action: '',
        }
      };
    }
  }

  addInfoTable2(data) {
    const formData = {
      point: data.value.point,
      importance: data.value.importance,
      observation: data.value.observation,
      action: data.value.action,
    }
    this.table2Info = [...this.table2Info, formData];
    console.log(this.table2Info);
  }

  captureScreen() {
    const restorPage = document.body.innerHTML;
    const printContent = document.querySelector('#content').innerHTML;
    document.body.innerHTML = printContent;
    window.print();
    // document.body.innerHTML = restorPage;
    window.location.reload();
  }

  ngOnInit() {
  }

}
