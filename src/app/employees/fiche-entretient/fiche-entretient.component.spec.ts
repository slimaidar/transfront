import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheEntretientComponent } from './fiche-entretient.component';

describe('FicheEntretientComponent', () => {
  let component: FicheEntretientComponent;
  let fixture: ComponentFixture<FicheEntretientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheEntretientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheEntretientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
