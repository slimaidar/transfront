import { ClientTabState, clientTabReducer } from './../PO_FACTURES/store/tab-tables/client-tab.reducer';
import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import {clientReducer, ClientState} from '../PO_FACTURES/store/client.reducer';

export interface State {
  clients: ClientState,
  clientTab: ClientTabState
}

export const reducers: ActionReducerMap<State> = {
  clients: clientReducer,
  clientTab: clientTabReducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
