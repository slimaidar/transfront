import {FactureIndividuelle} from './facture-individuelle';

export class FactureMensuelle {
  constructor(
    public id: number,
    public no: string,
    public fi: FactureIndividuelle,
    public date: Date,
    public montant: number
  ) {}
}
