export class Contact {
  constructor(
    public id: number,
    public title: string,
    public mail: string,
    public phone: string,
    public fax: string,
    public account: string
  ) {}
}
