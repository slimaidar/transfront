import {CliJobs} from '../../project/models/cli-jobs';

export class FactureIndividuelle {
  constructor(
    public id: number,
    public no,
    public cliJob: CliJobs,
    public montant: number,
    public date: Date,
    public status: string
  ) {}
}
