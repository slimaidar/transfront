import {FactureMensuelle} from './facture-mensuelle';
import {Client} from '../../clients/models/client';

export class Paiment {
  constructor(
    public id: number,
    public no: string,
    public factures: Array<FactureMensuelle>,
    public client: Client,
    public date: Date,
    public montant: number,
    public status: boolean
  ) {}
}
