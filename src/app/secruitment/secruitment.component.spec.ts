import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecruitmentComponent } from './secruitment.component';

describe('SecruitmentComponent', () => {
  let component: SecruitmentComponent;
  let fixture: ComponentFixture<SecruitmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecruitmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecruitmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
