import {Country} from '../../clients/models/country';
import {Currency} from '../../clients/models/currency';

export class Branch {
  constructor(
    public  id: number,
    public name?: string,
    public shortName?: string,
    public street1?: string,
    public street2?: string,
    public city?: string,
    public zipCode?: string,
    public state?: string,
    public country?: Country,
    public website?: string,
    public email?: string,
    public phone?: string,
    public fax?: string,
    public prefixes?: string,
    public currency?: Currency,

  ) {}
}
